<?php
class query{
    protected $dbc;
    function __construct($dbc){
        $this->dbc = $dbc;
    }

    public function query($query){
        if($result = $this->dbc->query($query)){
            return $result;
        }
        else{
            echo "something went wrong with the query";
        }
    }
}
?>