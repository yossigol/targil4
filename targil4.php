<?php
include "db.php";
include "query.php";
?>
<html>
<head>
<title>Booklist</title>
</head>

<body>
<?php
$db = new DB('localhost','intro','root','');
$dbc = $db->connect();
$query = new query($dbc);
$q = "SELECT b.author As author, b.title As title, u.name As user
    FROM books b
    JOIN users u
    ON b.user_id = u.id";
$result = $query->query($q);
echo '<br>';
if($result->num_rows>0){
    echo '<table>';
    echo '<tr><th>Author</th><th>Title</th><th>User</th></tr>';
    while($row = $result->fetch_assoc()){
        echo '<tr>';
        echo '<td>' .$row['author'].'</td><td>'.$row['title'].'</td><td>'.$row['user'].'</td>';
        echo '</tr>';
    }
    echo '</table>';
} else {
    echo "sorry no results";
}

?>
</body>
</html>