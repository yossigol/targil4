<?php
class DB{
    protected $db_name;
    protected $db_user;
    protected $db_pass;
    protected $db_host;
    protected $db;
    function __construct($db_host,$db_name,$db_user,$db_pass){
        $this->db_host = $db_host;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->db_name = $db_name;
    }

    public function connect(){
        $this->db = new mysqli($this->db_host,$this->db_user,$this->db_pass,$this->db_name);
        if(mysqli_connect_errno()){
            printf("Connection failed %s",mysqli_connect_error());
        }
        return $this->db;
    }
}
?>